import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Product from './Product'
import products from './products'

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            products: products
        };
    };
    componentDidMount(){
        this.setState({products: products})
    }

    handleChangeUnitRatio=(productId, unitRatio)=>{
        this.setState({
            products: this.state.products.map(stateProduct =>
                (stateProduct.productId !== productId) ?
                    stateProduct :
                    {
                        ...stateProduct,
                        unitRatio: unitRatio
                    }
            )
        })
    };
    
    handleCalculate=(productId, amount)=> {
        console.log(productId, amount)
    }

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
          <div className='listOfProductsWrapper'>
              <div className='listOfProducts'>
                  {this.state.products.map(product =>
                    <Product key={product.productId}
                         {...product} />
                  )}
              </div>
            </div>
      </div>
    );
  }
}

export default App;
