import React, { Component } from 'react';


export default class  Product extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            productCount: 1,
            unit: true
        };
    }
    
    handleChangeCount=(event)=>{
        if(event.target.value > 0) {
            return this.setState({ productCount: parseInt((event.target.value),10)});
        } else { return this.setState({ productCount: 1})}
    }
    
    handleIncrement=()=>{
        this.setState({productCount: this.state.productCount +1});
    }
    
    handleDecrement=()=>{
        if(this.state.productCount > 1) {
            return  this.setState({productCount: this.state.productCount -1});
        }
    }
    
    render(){
        const { primaryImageUrl, code, isActive, title, description, priceGoldAlt,
                priceGold, priceRetailAlt, priceRetail, bonusAmount} =this.props;
        
        
        return(
            <div className='productItem' >
                <img src={primaryImageUrl.replace(".jpg", "_220x220_1.jpg")} className='productImage' alt={title}/>
                <div className='descriptionProductItem'>
                    <div className='codeAndIsActiveProductItem'>
                        <span>Код: {code}</span>
                        <span className={isActive ? 'activeProductItem' : 'NotActiveProductItem'}>
                            {isActive? 'Наличие' : 'Нет в наличии'}
                        </span>
                    </div>
                    <div className='descriptionAndTitleProductItem'>
                        <p><b>{title}</b></p>
                        <p>{description}</p>
                    </div>
                </div>
                <div className='priceProductItem'>
                    <div className='priceGoldAltRow'>
                        <span className='priceGoldAlt'>По карте<br/>клуба</span>
                        <span className='priceGoldAltAmount'> {this.state.unit ? (this.state.productCount * priceGoldAlt).toFixed(2) :
                            (this.state.productCount * priceGold).toFixed(2)} &#8381;</span>
                    </div>

                    <span className='priceGold'> {this.state.unit ? (this.state.productCount * priceRetailAlt).toFixed(2) :
                            (this.state.productCount * priceRetail).toFixed(2)} &#8381;</span>

                    <span className='bonusAmountProductItem'> Можно купить за {(this.state.productCount * bonusAmount).toFixed(2)} балла</span>

                    <div className='buttonsUnitRatioProductItem'>
                        <button className={this.state.unit ? 'selectedButtonProductItem' : 'unselectedButtonProductItem'}
                                onClick={()=> this.setState({unit: true})}>За м.кв</button>
                        <button className={!this.state.unit ? 'selectedButtonProductItem' : 'unselectedButtonProductItem'}
                                onClick={()=> this.setState({unit: false})}>За упаковку</button>
                    </div>

                    <div className='infoProductItem'>

                        <span className='infoProductItemText'> Продается упаковками: <br/> 1 упак = 2.47 м. кв. </span>
                    </div>
                    
                    <div className='cartAndStepperWrapper'>

                        <div className='stepper'>
                            <input type='text' 
                                   value={this.state.productCount}
                                   onChange={this.handleChangeCount}
                                   className='productCountStepper'/>
                            
                            <div className='steppersIncDecr'>
                                <button className='stepperArrow' 
                                        onClick={this.handleIncrement}></button>
                                <button className='stepperArrow' 
                                        onClick={this.handleDecrement}></button>
                            </div>
                        </div>
                        
                        <button className='btn_cart'>в корзину</button>
                    </div>
                </div>
            </div>
        )
}



};